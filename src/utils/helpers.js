import {checkMultiple, requestMultiple, PERMISSIONS, RESULTS} from 'react-native-permissions';



export const isPermissionGranted = async () => {
    const permissions = await checkMultiple([PERMISSIONS.ANDROID.CAMERA,
                                             PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
                                             //PERMISSIONS.ANDROID.READ_SMS,
                                             //PERMISSIONS.ANDROID.RECEIVE_SMS,
                                             //PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
                                            ])

    return (
        permissions[PERMISSIONS.ANDROID.CAMERA] === RESULTS.GRANTED &&
        permissions[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] === RESULTS.GRANTED
        //permissions[PERMISSIONS.ANDROID.READ_SMS] === RESULTS.GRANTED &&
        //permissions[PERMISSIONS.ANDROID.RECEIVE_SMS] === RESULTS.GRANTED &&
        //permissions[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === RESULTS.GRANTED
    )
}

export const requestPermissions = async () => {
    const permissions = await requestMultiple([PERMISSIONS.ANDROID.CAMERA,
                                             PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
                                             //PERMISSIONS.ANDROID.READ_SMS,
                                             //PERMISSIONS.ANDROID.RECEIVE_SMS,
                                             //PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
                                            ])
    return (
        permissions[PERMISSIONS.ANDROID.CAMERA] === RESULTS.GRANTED &&
        permissions[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] === RESULTS.GRANTED 
        //permissions[PERMISSIONS.ANDROID.READ_SMS] === RESULTS.GRANTED &&
        //permissions[PERMISSIONS.ANDROID.RECEIVE_SMS] === RESULTS.GRANTED &&
        //permissions[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === RESULTS.GRANTED
    )
}