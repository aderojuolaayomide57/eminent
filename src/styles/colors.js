export const BLACK_COLOR = 'black';
export const PRIMARY_COLOR = '#b50000';
export const BACKGROUND_COLOR = '#E5E5E5';
export const CHARCOAL_BLACK = '#444F60';
export const GREY_BLACK = "#848d95e8"
export const WHITE_COLOR = 'white';
export const BORDER_COLOR = '#E6E9ED';
export const LINK_COLOR = "#cc6600";
export const DISABLED_TEXT = '#99a1b3';
export const WHITE_OAT_COLOR='#f2f7fa';
export const ORANGE_COLOR='orange';
export const LIGHT_BORDER_COLOR = '#ddd';





export const Colors_default = {	
	"datepicker_header": "#222",
	"datepicker_txt": "#ddd",
	"datepicker_txt_active": "#ddd",	
	"datepicker_txt_adj": "#ccc",
	"datepicker_current": "#555",	
	"datepicker_adj": "#888",
	"datepicker_active": "#000",
};

export const Colors_greenish = {
	"datepicker_header": "#007E33",
	"datepicker_txt": "#eee",
	"datepicker_txt_active": "#ddd",	
	"datepicker_txt_adj": "#ddd",
	"datepicker_current": "#66bb6a",	
	"datepicker_adj": "#9ccc65",
	"datepicker_active": "#1b5e20",
};

export const Colors_bluish = {
	"datepicker_header": "#1565c0",
	"datepicker_txt": "#eee",
	"datepicker_txt_active": "#ddd",	
	"datepicker_txt_adj": "#ddd",
	"datepicker_current": "#2196f3",	
	"datepicker_adj": "#90caf9",
	"datepicker_active": "#1a237e",
};

export const Colors_reddish = {	
	"datepicker_header": "#c62828",
	"datepicker_txt": "#eee",
	"datepicker_txt_active": "#ddd",	
	"datepicker_txt_adj": "#ddd",
	"datepicker_current": "#ef5350",	
	"datepicker_adj": "#ef9a9a",
	"datepicker_active": "#b71c1c",
};

