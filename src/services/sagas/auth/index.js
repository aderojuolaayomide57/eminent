import { call, put, takeLeading } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert, ToastAndroid, Platform } from 'react-native';

import { Types, Creators } from '../../redux/auth/actions';
import { navigate } from '../../../utils/navigation';
//import firebase from 'firebase';
import {db, auth} from '../../firebase/config';

import { 
    GoogleAuthProvider, 
    getAuth, 
    signInWithPopup, 
    signInWithEmailAndPassword, 
    createUserWithEmailAndPassword, 
    sendPasswordResetEmail, 
    signOut
} from 'firebase/auth';
import { query, getDocs, collection, where, addDoc } from "firebase/firestore";


const usersCollectionRef = collection(db, 'users');


const notifyMessage = (msg) => {
  const message = 'auth/user-not-found' === msg ? 'Wrong email or password' : msg
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, ToastAndroid.CENTER, ToastAndroid.LONG)
  } else {
    Alert.alert(message);
  }
}

const handleAuthentication = () => {
      signOut(auth)
}


  export function* register(actions) {
    try {
      const { data } = actions;
      const {fullname, email, password, phonenumber} = data;
      const register = e => createUserWithEmailAndPassword(auth, email, password)
      const response = yield call (register);
      const newres = addDoc(usersCollectionRef, {
        uid: response.user.uid,
        fullname: fullname,
        password,
        phonenumber,
        email,
        authProvider: "local",
      });
      const user = {uid: response.user.uid,fullname: fullname, email: email}
      yield put(Creators.registerSuccess({payload: user}));
      navigate('Home')
    } catch (error) {
      notifyMessage(error.code)
      yield put(Creators.registerFailure(error));
    }
  }
  
export function* watchRegister() {
    yield takeLeading(Types.REGISTER_REQUEST, register);
}

export function* login(actions) {
    try {
        const {email, password} = actions.data;
        const loginProcess = e => signInWithEmailAndPassword(auth, email, password);
        const response = yield call (loginProcess);
        //const q = query(usersCollectionRef, where("uid", "===", response.user.uid));
        const q = query(collection(db, "users"), where("uid", "==", response.user.uid));
        const docs = e => getDocs(q);
        const newres = yield call (docs);
        //const querySnapshot = getDocs(q);
        //yield put(Creators.loginSuccess({payload: response.user}));
        //navigate('Home')
        console.log("ssssssssssssssssssssssssssnewresssssssss");
        console.log(newres.docs._firestore)
    }catch (error) {
        notifyMessage(error.code)
        yield put(Creators.loginFailure(error.code));
    }
}

export function* watchLogin() {
    yield takeLeading(Types.LOGIN_REQUEST, login)
}


export function* logout(actions) {
    try {
        const logoutProcess = e => signOut(auth);
        const response = yield call (logoutProcess);
        yield put(Creators.logoutSuccess(null));
        navigate('Login')
    }catch (error) {
        console.log(error);
    }
}

export function* watchLogout() {
    yield takeLeading(Types.LOGOUT_REQUEST, logout)
}

  
