import { createTypes } from 'reduxsauce';


export default createTypes(
    `
    REGISTER_REQUEST
    REGISTER_SUCCESS
    REGISTER_FAILURE

    LOGIN_REQUEST
    LOGIN_SUCCESS
    LOGIN_FAILURE

    LOGOUT_REQUEST
    LOGOUT_SUCCESS
    LOGOUT_FAILURE

    `,
    {}
) 