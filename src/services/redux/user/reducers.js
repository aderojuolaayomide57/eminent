import { createReducer } from 'reduxsauce';
import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Types from './actionTypes';


export const INITIAL_STATE = {

}

export const HANDLERS = {

}

const persistConfig = {
    key: 'user',
    storage: AsyncStorage,
    blacklist: ['']

}

const userReducer = createReducer(INITIAL_STATE, HANDLERS);
export default persistReducer(userReducer, persistConfig);


