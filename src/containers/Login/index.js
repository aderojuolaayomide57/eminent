import { connect } from "react-redux";
import { Creators } from "../../services/redux/auth/actions";
import LoginScreen from '../../screens/Login';


const mapStateToProps = (state) => {
    const {isLogingin, error, data} = state.auth;
    return {
        isLogingin,
        error,
        data
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: data => {
            dispatch(Creators.loginRequest(data));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);