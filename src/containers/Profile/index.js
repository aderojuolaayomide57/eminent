import { connect } from 'react-redux';

import { Creators } from '../../services/redux/auth/actions';
import ProfileScreen from '../../screens/Tab/Profile';

const mapStateToProps = (state) => {
    const { isLoggingOut, error, data } = state.auth;
    return {
        isLoggingOut,
        data,
        error
    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        logout: () => {
            dispatch(Creators.logoutRequest())
        }
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)