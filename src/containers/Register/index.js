import { connect } from 'react-redux';

import { Creators } from '../../services/redux/auth/actions';
import RegisterScreen from '../../screens/Register';

const mapStateToProps = (state) => {
    const { isRegistering, error, data } = state.auth;
    return {
        isRegistering,
        data,
        error
    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        registerUser: data => {
            dispatch(Creators.registerRequest(data))
        }
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)