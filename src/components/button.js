import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native'
import PropTypes from 'prop-types';

import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    GREY_BLACK,
} from '../styles/colors';

const styles = StyleSheet.create({
    baseBtn: {
        borderRadius: 10,
        padding: 15,
    },
    addIcon: {
        
    },
    baseTxt: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 22,
        textAlign: 'center',
        letterSpacing: 0.33913
    },
    disabledStyle: {
        opacity: 0.3
    },

    linkText: {
        color: GREY_BLACK
    },

    primaryBtn: {
        backgroundColor: PRIMARY_COLOR
    },
    primaryText: {
        color: WHITE_COLOR
    },

    secondaryBtn: {
        backgroundColor: WHITE_COLOR
    },
    secondaryText: {
        color: PRIMARY_COLOR
    },

    borderedBtn: {
        borderWidth: 1,
        borderColor: PRIMARY_COLOR
    },
    borderedText: {
        color: PRIMARY_COLOR
    },
    
})

class PrimaryButton {
    constructor() {
        this.buttonType = 'primary'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: styles.primaryBtn,
            text: styles.primaryText
        }
    }
}

class SecondaryButton {
    constructor() {
        this.buttonType = 'secondary'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: styles.secondaryBtn,
            text: styles.secondaryText
        }
    }
}

class BorderedButton {
    constructor() {
        this.buttonType = 'bordered'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: styles.borderedBtn,
            text: styles.borderedText
        }
    }
}

class LinkButton {
    constructor() {
        this.buttonType = 'link'
    }

    isMatch(buttonType) {
        return buttonType === this.buttonType
    }

    buttonStyle() {
        return {
            btn: null,
            text: styles.linkText
        }
    }
}



const CustomButton = (props) => {
    const buttonVarieties = [
        new PrimaryButton(),
        new SecondaryButton(),
        new BorderedButton(),
        new LinkButton(),
    ]
    let selectedButton;
    for (const button of buttonVarieties) {
        if (button.isMatch(props.type)) {
            selectedButton = button.buttonStyle();
            break;
        }
    }
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.baseBtn, selectedButton.btn, props.style.btn, props.disabled && styles.disabledStyle]}
            disabled={props.disabled}
        >
            <Text style={[styles.baseTxt, selectedButton.text, props.style.text]}>{props.text}</Text>    
        </TouchableOpacity>
    )
}

CustomButton.propTypes = {
    type: PropTypes.oneOf(['primary', 'secondary', 'bordered', 'link']).isRequired,
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.shape({}),
    disabled: PropTypes.bool,
}


CustomButton.defaultProps = {
    style: {
        btn: null,
        text: null
    },
    disabled: false,
}

export default CustomButton;
