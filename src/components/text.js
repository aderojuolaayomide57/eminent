import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { CHARCOAL_BLACK } from '../styles/colors';

const style = StyleSheet.create({
    base: {
        color: CHARCOAL_BLACK,
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        letterSpacing: 0.33913
    },
    xs: {
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 16,
    },
    sm: {
        fontSize: 16,
        fontWeight: '900',
        lineHeight: 25
    },
    md: {
        fontSize: 24,
        lineHeight: 33,
        fontWeight: '900'
    },
    lg: {
        fontSize: 36,
        lineHeight: 49,
        fontWeight: '900'
    }
})

const faintedStyles = {
    text: {
        color: CHARCOAL_BLACK,
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 16,
        letterSpacing: 0.74
    }
}

const descriptionStyles = {
    text: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center',
        letterSpacing: 0.4,
        color: CHARCOAL_BLACK,
    }
}

const CustomText = props => {
    let size;
    switch(props.size) {
        case 'xs':
            size = style.xs;
            break;
        case 'sm':
            size = style.sm;
            break;
        case 'md':
            size = style.md;
            break;
        case 'lg':
            size = style.lg;
            break;
    }

    return (
        <Text style={[style.base, size, props.style]}>{props.text}</Text>
    )
}

export const CustomFaintedText = props => {
    return (
        <Text style={[faintedStyles.text, props.style]}>{props.text.toUpperCase()}</Text>
    )
}

export const CustomDescriptTionText = props => {
    return (
        <Text style={[descriptionStyles.text, props.style]}>{props.text}</Text>
    )
}

CustomText.propTypes = {
    size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']).isRequired,
    text: PropTypes.string
}

CustomText.defaultProps = {
    text: null
}

export default CustomText;
