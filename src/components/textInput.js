import React from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { BORDER_COLOR, CHARCOAL_BLACK, PRIMARY_COLOR, CHARCOAL_BLACK_LIGHT, GREY_BLACK } from '../styles/colors';
import CustomText from './text';


const styles = StyleSheet.create({
    inputWrapper: {
        borderRadius: 8,
        borderColor: BORDER_COLOR,
        borderWidth: 1.5,
        paddingTop: 25,
        paddingHorizontal: 5,
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 0.74,
        textAlignVertical: 'top',
        paddingBottom: 0
    },
    noTopBorder: {
        paddingTop: 25,
        paddingHorizontal: 5,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomColor: BORDER_COLOR,
        color: PRIMARY_COLOR,
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 0.74,
        textAlignVertical: 'top',
        paddingBottom: 0
    },
    labelWrapper: {
        padding: 4,
        alignSelf: 'flex-start',
        top: 10,
    },
    label: {
        fontSize: 16,
        textAlign: 'center',
        color: GREY_BLACK,
        paddingRight: 10
    },
    disabledText: {
        color: CHARCOAL_BLACK_LIGHT,
    },
    notDisabledText: {
        color: CHARCOAL_BLACK,
    }
})

const CustomTextInput = (props) => {
    return (
        <View style={styles.container}>
            {props.label && (
                <View style={styles.labelWrapper}>
                    <CustomText
                        size="xs"
                        style={styles.label}
                        text={props.label}
                    />
                </View>
            )}
            <TextInput
                style={[styles.inputWrapper,
                    props.noTopBorder ? styles.noTopBorder : {},
                    props.disabled ? styles.disabledText : styles.notDisabledText]}
                placeholder={props.placeholder}
                placeholderTextColor={props.placeholderColor || "rgba(68, 79, 96, 0.5)"}
                onChangeText={props.onChangeText}
                value={props.value}
                keyboardType={props.keyboardType}
                secureTextEntry={props.secureTextEntry}
                editable={!props.disabled}
                multiline={props.multiline}
                numberOfLines={props.numberOfLines}
            />
        </View>
    )
}

CustomTextInput.propTypes = {
    label: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string,
    keyboardType: PropTypes.oneOf(['default',
                                  'number-pad',
                                  'decimal-pad',
                                  'numeric',
                                  'email-address',
                                  'phone-pad']),
    secureTextEntry: PropTypes.bool,
    multiline: PropTypes.bool,
    numberOfLines: PropTypes.number,

}

CustomTextInput.defaultProps = {
    value: null,
    keyboardType: 'default',
    secureTextEntry: false,
    multiline: false,
}

export default CustomTextInput