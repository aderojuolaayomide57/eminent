import React from 'react'

import {View, StyleSheet} from 'react-native';


const Card = ({children, style}) => {
    return (
        <View style={styles.card}>
            {children}
        </View>
    )
}


const styles = StyleSheet.create({
    card: {
        flex: 1,
        backgroundColor:"white",
        elevation:3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        borderRadius: 10,
        marginBottom: 20,
        width: "90%",
        alignSelf: 'center',
        padding: 20

    },
})

export default Card;