import React from 'react'
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import CustomText from './text';

const ErrorMessage = ({ errorValue }) => (
  <View style={styles.container}>
    <CustomText
      size="xs"
      style={styles.errorText}
      text={errorValue} />
  </View>
)

ErrorMessage.propTypes = {
  errorText: PropTypes.string
}

ErrorMessage.defaultProps = {
  errorText: null
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 15
  },
  errorText: {
    color: 'red'
  }
})

export default ErrorMessage