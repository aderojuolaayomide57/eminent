import React, {useState} from 'react';
import {Picker, View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker'



import { BORDER_COLOR, CHARCOAL_BLACK, PRIMARY_COLOR, CHARCOAL_BLACK_LIGHT, GREY_BLACK } from '../styles/colors';
import CustomText from './text';


const styles = StyleSheet.create({
    container: {
        borderRadius: 8,
        borderColor: BORDER_COLOR,
        borderWidth: 1.5,
        paddingTop: 8,
        paddingHorizontal: 5,
    },
    inputWrapper: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: 0.74,
        textAlignVertical: 'top',
        paddingBottom: 0
    },
    labelWrapper: {
        padding: 4,
        alignSelf: 'flex-start',
        top: 10,
    },
    label: {
        fontSize: 16,
        textAlign: 'center',
        color: GREY_BLACK,
        paddingRight: 10
    },
    disabledText: {
        color: CHARCOAL_BLACK_LIGHT,
    },
    notDisabledText: {
        color: CHARCOAL_BLACK,
    }
})


const CustomSelect = (props) => {
    return (
        <View style={styles.container}>
            {props.label && (
                <View style={styles.labelWrapper}>
                    <CustomText
                        size="xs"
                        style={styles.label}
                        text={props.label}
                    />
                </View>
            )}
            <Picker
                selectedValue={props.selectedValue}
                style={styles.inputWrapper}
                onValueChange={props.onValueChange}
            >
                {props.list.map(data => {
                    return (
                        <Picker.Item key={data.value} label={data.label} value={data.value} />
                    )
                })}
            </Picker>
        </View>
    )
}

CustomSelect.propTypes = {
    label: PropTypes.string.isRequired,
    onValueChange: PropTypes.func.isRequired,
    selectedValue: PropTypes.string,
    list: PropTypes.array.isRequired,
}

export default CustomSelect;