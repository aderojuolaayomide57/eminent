import React from 'react';
import { NavigationContainer } from "@react-navigation/native";

import { createStackNavigator } from "@react-navigation/stack";
import { navigationRef } from './utils/navigation';
import Splash from './screens/Splash';
//import FirstTimer from './screens/FirstTimer';
import Login from './containers/Login';
import Register from './containers/Register';
import Dashboard from './screens/Tab';
import Artisan from './screens/Artisan';


const Stack = createStackNavigator();


const App = () => {
    return(
        <NavigationContainer ref={navigationRef}>
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen 
                    name="Splash" 
                    component={Splash} 
                    options={{
                        headerShown: false
                    }}
                />
                
                <Stack.Screen 
                    name="Login" 
                    component={Login} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Register" 
                    component={Register} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Home" 
                    component={Dashboard} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Artisan" 
                    component={Artisan} 
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default App;