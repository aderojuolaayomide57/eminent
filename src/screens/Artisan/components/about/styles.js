import { StyleSheet } from 'react-native';
import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    LIGHT_BORDER_COLOR,
    BLACK_COLOR
} from '../../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10

    },
    action: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 0,
        marginTop: 0
    },
    textHeader: {
        color: 'black', 
        fontWeight: 'bold'
    },
    icon: {
        color: LIGHT_BORDER_COLOR,
        fontSize: 20,
        marginRight: 10,
        paddingTop: 5
    },
    time: {
        borderRadius: 5,
        borderColor: PRIMARY_COLOR,
        borderWidth: 2,
        paddingLeft: 10,
        paddingRight: 10,
        color: BLACK_COLOR,
        backgroundColor: WHITE_COLOR,
        marginRight: 8,
        marginBottom: 15,
        
    },
    timeBox: {
        flexDirection: 'row',
        alignItems: "flex-start",
        flexWrap: 'wrap',
        flex: 1,
        marginTop: 15
    },
    box: {
        flex: 2
    }
 
    
});


export default styles;