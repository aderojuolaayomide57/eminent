import React from 'react';
import {View,Text,TouchableOpacity, ScrollView} from 'react-native';
import styles from './styles';
import CustomText from '../../../../components/text';
import CustomButton from '../../../../components/button';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const ActionInfo = (props) => {
    return(
        <View style={styles.action}>
            {props.type === 'MaterialCommunityIcons' && 
                <MaterialCommunityIcons name={props.icon} style={styles.icon} />}
            {props.type === 'FontAwesome' && <FontAwesome name={props.icon} style={styles.icon} />}
            {props.type === 'Entypo' && <Entypo name={props.icon} style={styles.icon} />}

            <CustomText 
                text={props.text}
                size="sm"
            />
        </View>
    );
}

const Time = (props) => (
    <TouchableOpacity style={styles.time} >
      <CustomText text={props.title} size="sm" style={{fontSize: 13}}/>
    </TouchableOpacity>
  )
  

const About = (props) => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.box}>
                <CustomText
                    text="Information"
                    size="sm"
                    style={styles.textHeader}
                />
                <CustomText
                    text="He's a known barber for her biconical barbershop,a line of hair products, creating different signatures."
                    size="sm"
                    //style={{color: 'black'}}
                />
            </View>
            <View style={styles.box}>
                <CustomText
                    text="Contact"
                    size="sm"
                    style={styles.textHeader}
                />
                <ActionInfo 
                    text="08123849329"
                    icon="phone"
                    type="FontAwesome"
                />
                <ActionInfo 
                    text="www.eminentsalson.com"
                    icon="web"
                    type="MaterialCommunityIcons"
                />
            </View>
            <View style={styles.box}>
                <CustomText
                    text="Address"
                    size="sm"
                    style={styles.textHeader}
                />
                <ActionInfo 
                    text="No. 15 Nelson Avenue, Olonu street opposite flex supermarket shop i201."
                    icon="location-pin"
                    type="Entypo"
                />
                
            </View>
            <View style={styles.box}>
                <CustomText
                    text="Available slot"
                    size="sm"
                    style={styles.textHeader}
                />
                <View style={styles.timeBox}>
                    <Time title="8:00am"/>
                    <Time title="9:00am"/>
                    <Time title="10:00am"/>
                    <Time title="11:00am"/>
                    <Time title="12:00am"/>  
                </View>
            </View>
            



            <CustomButton
              type="primary"
              text="BOOK APPOINTMENT"
              onPress={() => {}}
              style={{
                btn: {marginLeft: 20, marginRight: 20, marginBottom: 20}
              }}
            />
        </ScrollView>
    )
}

export default About;