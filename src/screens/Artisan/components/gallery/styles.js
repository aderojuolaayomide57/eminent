import { StyleSheet } from 'react-native';
import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    LIGHT_BORDER_COLOR,
    BLACK_COLOR
} from '../../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,

    },
    mainWrapper: {
        flexDirection: 'row',
        alignItems: "flex-start",
        flexWrap: 'wrap',
        flex: 1,
        paddingBottom: 20
    },
    itemWrapper: {
        flexDirection: 'column',
        alignItems: 'center',
        padding: 5
    },
    image: {
        width: 180,
        height: 180,
        borderWidth: 2,
        borderColor: WHITE_COLOR,
        borderRadius: 20
    },
    
});


export default styles;