import React from 'react';
import {ScrollView,View,TouchableOpacity, Image} from 'react-native';
import styles from './styles';


const Photo = (props) => {
    const { navigation: { navigate } } = props;
    return (
        <TouchableOpacity 
            style={styles.itemWrapper}
            onPress={() => navigate('PhotoDetails') }
        >
            <Image
                source={props.image}
                style={styles.image}
            />
        </TouchableOpacity>
    );
}

const Gallery = (props) => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.mainWrapper}>
                <Photo image={require("../../../../assets/image1.png")} {...props}/>
                <Photo image={require("../../../../assets/image3.png")} {...props}/>
                <Photo image={require("../../../../assets/image2.png")} {...props}/>
                <Photo image={require("../../../../assets/image1.png")} {...props}/>
                <Photo image={require("../../../../assets/image3.png")} {...props}/>
                <Photo image={require("../../../../assets/image1.png")} {...props}/>
            </View>
        </ScrollView>
    )
}

export default Gallery;