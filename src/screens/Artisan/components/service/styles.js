import { StyleSheet } from 'react-native';
import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    LIGHT_BORDER_COLOR,
    BLACK_COLOR
} from '../../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10


    },
    service: {
        flex: 1,
        borderBottomColor: LIGHT_BORDER_COLOR,
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10
    },
    text: {
        fontSize: 18, 
        fontWeight: 'bold'  
    }
 
    
});


export default styles;