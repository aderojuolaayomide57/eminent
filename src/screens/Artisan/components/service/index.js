import React from 'react';
import {ScrollView,View,TouchableOpacity} from 'react-native';
import styles from './styles';
import CustomText from '../../../../components/text';
import CustomButton from '../../../../components/button';


const ServiceBox = (props) => {
    return(
        <View style={styles.service}>
            <CustomText 
                text={props.text}
                size="md"
                style={{fontSize: 18, fontWeight: 'bold'}}
            />
        </View>
    );
}


const Service = (props) => {
    const { navigation: { navigate } } = props;
    return (
        <ScrollView style={styles.container}>
            <ServiceBox text="Haircut"/>
            <ServiceBox text="Waxing"/>
            <ServiceBox text="Dyeing"/>
            <ServiceBox text="Bread Trim"/>
            <ServiceBox text="Express Shave"/>
            <CustomButton
              type="primary"
              text="BOOK APPOINTMENT"
              onPress={() => {}}
              style={{
                btn: {marginTop: 30, marginBottom: 20, borderRadius: 5}
              }}
            />
        </ScrollView>
    )
}

export default Service;