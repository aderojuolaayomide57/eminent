import { StyleSheet } from 'react-native';
import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    LIGHT_BORDER_COLOR,
    BLACK_COLOR
} from '../../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10

    },
    reviewBox: {
        flex: 1,
        marginBottom: 25
    },
    name: {
        fontSize: 16,
        color: 'black', 
    },
    text: {
        fontSize: 15,
        marginLeft: 10,
    },
    icon: {
        fontSize: 14,
        color: LIGHT_BORDER_COLOR,
        lineHeight: 27
    },
    image: {
        width: 30,
        height: 30,
        borderRadius: 15,
        marginRight: 10,
    },
    rating: {
        flexDirection: 'row',
    },
    topBox: {
        flexDirection: 'row',
        marginBottom: 10
    },
    info: {

    }

 
    
});


export default styles;