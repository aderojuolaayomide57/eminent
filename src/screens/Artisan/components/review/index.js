import React from 'react';
import {View,Text,TouchableOpacity,Image, ScrollView} from 'react-native';
import styles from './styles';
import CustomText from '../../../../components/text';
import CustomButton from '../../../../components/button';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const ReviewBox = (props) => (
    <TouchableOpacity style={styles.reviewBox} >
        <View style={styles.topBox}>
            <Image source={props.image} style={styles.image}/>
            <CustomText 
                text={props.name}
                size="sm"
                style={styles.text}
            />
        </View>
        <View style={styles.rating}>
            <FontAwesome name="star" style={styles.icon}/>
            <FontAwesome name="star" style={styles.icon}/>
            <FontAwesome name="star" style={styles.icon}/>
            <FontAwesome name="star" style={styles.icon}/>
            <FontAwesome name="star" style={styles.icon}/>
            <CustomText 
                text={props.date}
                size="sm"
                style={styles.text}
            />
        </View>
        <View style={styles.info}>
            <CustomText 
                text={props.info}
                size="sm"
            />
        </View>
    </TouchableOpacity>
  )
  

const Review = (props) => {
    return (
        <ScrollView style={styles.container}>
            <ReviewBox 
                name="Quadri A."
                image={require("../../../../assets/image3.png")}
                date="11/11/21"
                info="this is great i reallly love it, this is a life savong app for everyone savong savong savong savong savongsavong savong"
            />
            <ReviewBox 
                name="Quadri A."
                image={require("../../../../assets/image3.png")}
                date="11/11/21"
                info="this is great i reallly love it, this is a life savong app for everyone"
            />
            <ReviewBox 
                name="Quadri A."
                image={require("../../../../assets/image3.png")}
                date="11/11/21"
                info="this is great i reallly love it, this is a life savong app for everyone"
            />

        </ScrollView>
    )
}

export default Review;