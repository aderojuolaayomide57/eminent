import React, {Component} from 'react';
import { View, ImageBackground, StatusBar } from 'react-native';
import styles from './styles';
import CustomText, {CustomDescriptTionText} from '../../components/text';
import { TabView, TabBar} from 'react-native-tab-view';
import About from './components/about';
import Service from './components/service';
import Gallery from './components/gallery';
import Review from './components/review';
import {PRIMARY_COLOR} from '../../styles/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

  



class Artisan extends Component {

    constructor (props) {
        super(props);
        this.setIndex = this.setIndex.bind(this)
        this.state = {
            index: 0,
            width: 0,
            routes: [
                { key: 'about', title: 'About' },
                { key: 'service', title: 'Service' },
                { key: 'gallery', title: 'Gallery' },
                { key: 'review', title: 'Review' },
              ]
        }
    }

      
      componentWillmount() {
        const layout = useWindowDimensions();

        //this.setState({width: window.innerHeight + 'px'});
        this.setState({width: layout.width});
      }
      
     renderTabBar = props => (
        <TabBar
          {...props}
          indicatorStyle={{backgroundColor: PRIMARY_COLOR}}
          style={{backgroundColor: 'white', borderBottomWidth: 2, borderBottomColor: "#ddd"}}
          activeColor={PRIMARY_COLOR}
          inactiveColor="#ddd"
          bounces={true}
          labelStyle={{fontWeight: "bold", fontSize: 16}}
        />
      );

    renderScene = ({ route }) => {
        switch (route.key) {
            case 'about':
                return <About {...this.props} />;
            case 'service':
                return <Service {...this.props} />;
            case 'gallery':
                return <Gallery {...this.props} />;
            case 'review':
                return <Review {...this.props} />;
            default:
                return null;
        }
    };

      

    setIndex = () => {
        this.setState({index: this.state.index++});
    }


    render() {
        const index = this.state.index
        const routes = this.state.routes
        return (
            <>
                <ImageBackground source={require("../../assets/Group894.png")} style={styles.background}>
                        <StatusBar
                            animated={true}
                            backgroundColor="transparent"
                            translucent
                            barStyle="light-content"
                            StatusBarAnimation="fade"
                            //showHideTransition={statusBarTransition}
                        />
                    <View style={styles.imageContainer}>
                        
                        <View>
                            <CustomText
                                text="King David"
                                size="lg"
                                style={styles.headerText}
                            />
                            <View style={styles.rating}>
                                <FontAwesome name="star" style={styles.icon} />
                                <CustomDescriptTionText
                                    text="5.0"
                                    style={styles.subText}
                                />
                                <CustomDescriptTionText
                                    text="Barber"
                                    style={styles.subText}
                                />
                            </View>
                        </View>
                        <View style={styles.headerIconBox}>
                            <CustomDescriptTionText
                                text="OPEN"
                                style={styles.available}
                            />
                        </View>
                    </View>
                </ImageBackground>
                <TabView
                    {...this.props}
                    navigationState={{ index, routes }}
                    renderTabBar={this.renderTabBar}
                    renderScene={this.renderScene}
                    onIndexChange={this.setIndex}
                    initialLayout={{ width: this.state.width }}
                    style={{
                        marginTop: -280,
                    }}
                />
            </>
                
        )
    }
}
export default Artisan;