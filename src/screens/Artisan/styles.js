import { StyleSheet } from 'react-native';
import { 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    ORANGE_COLOR
} from '../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
    },
    image: {
        alignSelf: 'center',
        width: 150,
        height: 100,
        justifyContent: 'center'  
    },
    imageContainer: {
        flex: 1,
        paddingTop: 185,
        paddingLeft: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 20,

    },
    background: {
        flex: 1,
        resizeMode: "cover",
        width: SCREEN_WIDTH,
        alignSelf: 'center',
        height: SCREEN_HEIGHT * 0.33,
        marginTop: -1
    },
    headerText: {
        color: WHITE_COLOR,
    },
    headerSubText: {
        color: WHITE_COLOR,
    },
    headerIconBox: {
        backgroundColor: ORANGE_COLOR,
        height: 21,
        width: 50,
        justifyContent: 'center',
        marginTop: 45,
        textAlign: 'center',
        borderRadius: 5
    },
    available: {
        color: WHITE_COLOR,
        fontWeight: 'bold',
        fontSize: 13,
        marginTop: 0,
        textAlign: 'center',
        lineHeight: 15

    },
    icon: {
        color: PRIMARY_COLOR,
        fontSize: 25,
        marginRight: 5,
    },
    tabContainer: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 5
    },
    subText: {
        marginLeft: 0,
        paddingLeft: 0,
        marginRight: 5,
        color: WHITE_COLOR,
        fontWeight: 'bold',
    },
    rating: {
        flexDirection: 'row'
    },
});


export default styles;