import React from 'react';
import { View } from 'react-native';
import CustomTextInput from '../../../components/textInput';
import CustomButton from '../../../components/button';
import ErrorMessage from '../../../components/errorMessage';
import { Formik } from 'formik'
import * as Yup from 'yup';


const validationSchema = Yup.object().shape({
    email: Yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Email is required'),
    password: Yup.string()
    .label('Password')
    .min(6, 'Password must be atleast 6 characters long')
    .required('Password is required'),
})

const initialValues = {
    email: '',
    password: '',
}



const LoginForm = (props) => {
    const { navigation: { navigate } } = props;
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={values => props.onSubmit(values)}
            validationSchema={validationSchema}
        >

            {formikProps => (
                <React.Fragment>
                <CustomTextInput
                    name="email"
                    label="Email"
                    placeholder="Enter your email address"
                    onChangeText={formikProps.handleChange('email')}
                    value={formikProps.values.email}
                    noTopBorder={true}
                    keyboardType="email-address"
                />
                <ErrorMessage errorValue={formikProps.errors.email} />
                <CustomTextInput
                    name="password"
                    label="Password"
                    placeholder="Enter your password."
                    onChangeText={formikProps.handleChange('password')}
                    value={formikProps.values.password}
                    noTopBorder={true}
                    secureTextEntry={true}
                />
                <ErrorMessage errorValue={formikProps.errors.password} />
                <View style={{ marginTop: -30, marginBottom: 40, alignSelf: 'flex-end' }}>
                    <CustomButton
                        type="link"
                        onPress={() => navigate('Register')}
                        text="Forgot Password?"
                    />
                </View>
                <CustomButton
                    type="primary"
                    text="LOGIN"
                    onPress={formikProps.handleSubmit}
                    disabled={props.isLoginIn}
                />
                </React.Fragment>
            )}

        </Formik>
    )
} 


export default LoginForm