import React, { Component } from 'react';
import { View, Image, Text, Animated } from 'react-native';


import styles from './styles';


export default class SplashScreen extends Component {

    constructor(props){
        super(props);

        this.LogoNameXY = new Animated.ValueXY({x: 0, y: 150});
        this.positionXY = new Animated.ValueXY({x:0, y:-200});
        this.TLogoXY = new Animated.ValueXY({x:-50, y:-100});
        this.ELogoXY = new Animated.ValueXY({x:100, y:-50});




        this.state = {
            //sizeXY: new Animated.Value(70), // initial size for each ball
            endValue: 0, // ending value size for each ball
            duration: 4000, // duration time for the ball
            
        }
    }
    componentDidMount() {
        Animated.timing(this.LogoNameXY, {
            //delay: 1000,
            toValue: {x: 0, y: 20}, // to position value x and y specified for each ball
            duration: 2000, // duration time for the ball to move to the point specified 
            useNativeDriver: true,
            velocity: 3,
            friction: 8,    
        }).start();
            Animated.timing(this.positionXY, {
                //delay: 1000,
                toValue: {x: 0, y: 0}, // to position value x and y specified for each ball
                duration: 2000, // duration time for the ball to move to the point specified 
                useNativeDriver: true,
                velocity: 3,
                friction: 8,    
            }).start();
            Animated.timing(this.TLogoXY, {
                delay: 1000,
                toValue: {x: -15, y: 0}, // to position value x and y specified for each ball
                duration: 2000, // duration time for the ball to move to the point specified 
                useNativeDriver: true,
                velocity: 3,
                friction: 8,    
            }).start();
            Animated.timing(this.ELogoXY, {
                delay: 1000,
                toValue: {x: 15, y: 0}, // to position value x and y specified for each ball
                duration: 1000, // duration time for the ball to move to the point specified 
                useNativeDriver: true,
                velocity: 3,
                friction: 8,    
            }).start();
        setTimeout(() => {
            this.props.navigation.navigate(this.getNextRoute())
        }, 7000)  ;

    }

    getNextRoute = () => {
        //let nextRoute = "FirstTimer"
        let nextRoute = "Login"
        if (this.props.token) {
            nextRoute = "Home"
        }
        return nextRoute;
    }

    render() {
        return (
            <View style={styles.container}>
                <Animated.View style={[
                    styles.logo, 
                    {
                        transform: [{translateX: this.positionXY.x},{translateY: this.positionXY.y}]
                    },                
                ]} 
                >
                    <Animated.Text style={[
                        styles.logoT,
                        {
                            transform: [{translateX: this.TLogoXY.x},{translateY: this.TLogoXY.y}]
                        },
                    ]}
                    >
                        T
                    </Animated.Text>
                    <Animated.Text style={[
                        styles.logoE,
                        {
                            transform: [{translateX: this.ELogoXY.x},{translateY: this.ELogoXY.y}]
                        },
                    ]}
                    >
                        E
                    </Animated.Text>
                </Animated.View>
                <Animated.View 
                    style={[
                        styles.logoBox,
                        {
                            transform: [{translateX: this.LogoNameXY.x},{translateY: this.LogoNameXY.y}]
                        },
                    ]}
                >
                    <Text style={styles.logoTextLeft}>THE</Text>
                    <Text style={styles.logoTextMiddle}>EMINENT</Text>
                    <Text style={styles.logoTextRight}>TM</Text>
                </Animated.View>
            </View>
        )
    }
}
