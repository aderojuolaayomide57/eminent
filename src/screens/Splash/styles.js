import React from 'react';
import { StyleSheet } from 'react-native';
import { BLACK_COLOR, WHITE_COLOR, PRIMARY_COLOR } from '../../styles/colors';



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: BLACK_COLOR
    },
    image: {
        height: 150,
        width: 200,
        resizeMode: 'contain'
    },
    logo: {
        width: 100,
        height: 100,
        //alignSelf: 'center',
        flexDirection: 'row',
        borderRadius: 50,
        borderWidth: 2,
        borderColor: WHITE_COLOR,
        justifyContent: 'center',
    },
    logoT: {
        fontSize: 60,
        color: WHITE_COLOR,
        fontWeight: "bold",
        position: 'absolute'
    },
    logoE: {
        color: PRIMARY_COLOR,
        fontSize: 60,
        fontWeight: "bold",
        paddingTop: 13,
        marginLeft: -5,
        position: 'absolute'
    },
    logoBox: {
        alignSelf: 'center',
        flexDirection: 'row',
    },
    logoTextLeft: {
        fontSize: 45,
        color: WHITE_COLOR,
        fontWeight: "bold",
        marginRight: 10
    },
    logoTextMiddle: {
        color: PRIMARY_COLOR,
        fontSize: 45,
        fontWeight: "bold",
    },
    logoTextRight: {
        color: WHITE_COLOR,
        fontSize: 15,
        fontWeight: "bold",
    }
});

export default styles;