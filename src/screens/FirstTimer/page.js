import React from 'react';

import { View, ImageBackground } from 'react-native';
import styles from './styles';
import CustomButton from '../../components/button';
import CustomText from '../../components/text';


const Page = (props) => {
    return (
        <ImageBackground source={props.image} style={styles.background}>
            <View style={styles.innerContainer}>
                <View style={styles.textContainer}>
                    <CustomText 
                        text={props.title}
                        size="lg"
                        style={styles.imageHeaderText}
                    />
                    <CustomText 
                        text={props.subTitle}
                        size="sm"
                        style={styles.imageSubText}
                    />
                </View>
                <CustomButton
                    type="primary"
                    text={props.buttonText}
                    onPress={() => props.rightButtonPress()}
                    style={{
                        btn: styles.btnStyle,
                        text: {
                            fontWeight: "bold",
                            fontSize: 22
                        }
                    }}
                />
            </View>
        </ImageBackground>
    )
}

export default Page;