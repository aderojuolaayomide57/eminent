import React, { useRef, useEffect } from 'react';
import { View } from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import Page from './page';
import {db, auth} from '../../services/firebase/config';
import {collection, getDocs, addDoc} from 'firebase/firestore';
import { LogBox } from 'react-native';





const FirstTimer = (props) => {
    /**LogBox.ignoreLogs(['Setting a timer']);
    const usersCollectionRef = collection(db, 'users'); **/

    const pagerRef = useRef(null);

    const handlePageChange = pageNumber => {
        pagerRef.current.setPage(pageNumber);
    };

    /**useEffect(() => {
        const getUsers = async () => {
            const data = await getDocs(usersCollectionRef)
            console.log('data');
            console.log(data.docs);
        }

        const createUser = async () => {
            const done = await addDoc(usersCollectionRef, {
                //uid: 'response.user.uid',
                fullname: 'ayomide',
                phonenumber: '08147643756',
                password: 'whgvdxhas cda',
                //authProvider: "local",
                email: 'aderojuolaayomide57@gmail.com',
            });

            console.log("done")
            console.log(done)
        }

        //getUsers();
        createUser();
    });**/



    return(
        <View style={{ flex: 1 }}>
            <ViewPager style={{ flex: 1 }} initialPage={0} ref={pagerRef}>
                <View key="1">
                    <Page
                        title="Book us today"
                        subTitle="Hair without queue"
                        image={require("../../assets/OnBoarding.png")}
                        buttonText='Next'
                        rightButtonPress={() => {
                            handlePageChange(1);
                        }}
                    />
                </View>
                <View key="2">
                    <Page
                        title="Choose your favourite hairstyle"
                        subTitle="Let's be your hair stylist"
                        image={require("../../assets/OnBoarding1.png")}
                        buttonText='Next'
                        rightButtonPress={() => {
                            handlePageChange(2);
                        }}
                    />
                </View>
                <View key="3">
                    <Page
                        title="Enjoy your hair treatment"
                        subTitle="Sit back and relax while we makes you look happier every day"
                        image={require("../../assets/OnBoarding2.png")}
                        buttonText='Get Started'
                        rightButtonPress={() =>  props.navigation.navigate('Login')}
                    />
                </View>
            </ViewPager>
        </View>
  
    )
}


export default FirstTimer;