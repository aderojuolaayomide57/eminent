import { StyleSheet } from 'react-native';
import { PRIMARY_COLOR, WHITE_COLOR } from '../../styles/colors';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../../styles/base';

const styles = StyleSheet.create({
    imageWrapper: {
        justifyContent: 'center',
        alignSelf: 'center',
        width: SCREEN_WIDTH * 0.9,
    },
    imageHeaderText: {
        color: WHITE_COLOR,
    },
    imageSubText: {
        color: WHITE_COLOR,
        marginBottom: 20,
        fontSize: 20
    },
    btnStyle: {
        borderRadius: 10,
        padding: 15,
    },
    background: {
        flex: 1,
        resizeMode: "cover",
        width: SCREEN_WIDTH,
        alignSelf: 'center',
        height: SCREEN_HEIGHT
    },
    innerContainer: {
        marginTop: 'auto',                                                  
        padding: 10,
        paddingBottom: 20
    },
    textContainer: {
        width: SCREEN_WIDTH * 0.80
    }
})

export default styles;


