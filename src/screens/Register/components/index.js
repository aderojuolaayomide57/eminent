import React from 'react';
import { View, ScrollView, Image } from 'react-native';
import CustomTextInput from '../../../components/textInput';
import CustomButton from '../../../components/button';
import ErrorMessage from '../../../components/errorMessage';
import {Formik} from 'formik'
import * as Yup from 'yup';


const validationSchema = Yup.object().shape({
    email: Yup.string()
      .label('Email')
      .email('Enter a valid email')
      .required('Email is required'),
    fullname: Yup.string()
      .label('Full Name')
      .required('Full name is required'),
      phonenumber: Yup.string()
      .label('Phone Number')
      .required('Phone number is required')
      .min(11, 'Phone number is not complete')
      .max(11),
    password: Yup.string()
    .label('Password')
    .min(6, 'Password must be atleast 6 characters long')
    .required('Password is required'),
})

const initialValues = {
    email: '',
    fullname: '',
    password: '',
    phonenumber: '',
}



const RegisterForm = (props) => {
    return(
        <Formik
            initialValues={initialValues}
            onSubmit={values => props.onSubmit(values)}
            validationSchema={validationSchema}
        >
          {formikProps => (
              <React.Fragment>
                <CustomTextInput
                    name="fullname"
                    label="Full Name"
                    placeholder="Enter your full name"
                    onChangeText={formikProps.handleChange('fullname')}
                    value={formikProps.values.fullname}
                    noTopBorder={true}
                />
                <ErrorMessage errorValue={formikProps.errors.fullname} />
                <CustomTextInput
                    name="email"
                    label="Email"
                    placeholder="Enter your email address"
                    onChangeText={formikProps.handleChange('email')}
                    value={formikProps.values.email}
                    noTopBorder={true}
                    keyboardType="email-address"
                />
                <ErrorMessage errorValue={formikProps.errors.email} />
                <CustomTextInput
                    name="phonenumber"
                    label="Phone Number"
                    placeholder="Enter your phone number"
                    onChangeText={formikProps.handleChange('phonenumber')}
                    value={formikProps.values.phonenumber}
                    keyboardType="phone-pad"
                    noTopBorder={true}
                />
                <ErrorMessage errorValue={formikProps.errors.phonenumber} />
                <CustomTextInput
                    name="password"
                    label="Password"
                    placeholder="Enter your password."
                    onChangeText={formikProps.handleChange('password')}
                    value={formikProps.values.password}
                    noTopBorder={true}
                    secureTextEntry={true}
                />
                <ErrorMessage errorValue={formikProps.errors.password} />
                <CustomButton
                    type="primary"
                    text="REGISTER"
                    onPress={formikProps.handleSubmit}
                    disabled={props.isLoginIn}
                    style={{
                      btn: {marginTop: 10}
                    }}
                />
              </React.Fragment>
            )}
        </Formik>
    )
} 


export default RegisterForm;
