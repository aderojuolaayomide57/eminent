import React from 'react';
import { View, ScrollView, Image } from 'react-native';
import RegisterForm from './components';
import CustomButton from '../../components/button';
import CustomText from '../../components/text';
import styles from './styles';
import { GREY_BLACK, LINK_COLOR } from '../../styles/colors'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


const RegisterScreen = (props) => {
    const { navigation: { navigate } } = props;
    return(
        <KeyboardAwareScrollView 
          style={styles.container} 
          extraScrollHeight={60} 
          enableOnAndroid
        >
            <View style={styles.imageContainer}>
                <Image 
                    style={styles.image} 
                    source={require('../../assets/logo.png')} 
                />
            </View>
            <View style={styles.formContainer}>
                <RegisterForm
                    onSubmit={props.registerUser}
                    {...props}
                />
                <View style={styles.bottomTextContainer}>
                    <CustomText 
                        text="Already have an Account?"
                        size='sm'
                        style={{
                            color: GREY_BLACK,
                            lineHeight: 10,
                            paddingTop: 28,
                            fontSize: 18
                        }}
                    />
                    <CustomButton
                        type="link"
                        onPress={() => navigate('Home')}
                        text="Sign In"
                        style={{
                            btn: {paddingLeft: 5},
                            text: {color: LINK_COLOR}
                        }}
                    />
                </View>
            </View>
        </KeyboardAwareScrollView>
    )
} 


export default RegisterScreen;
