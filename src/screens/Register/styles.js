import { StyleSheet } from 'react-native';
import { BLACK_COLOR, PRIMARY_COLOR } from '../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        alignSelf: 'center',
        width: 150,
        height: 100,
        justifyContent: 'center'  
    },
    imageContainer: {
        flex: 1,
        backgroundColor: BLACK_COLOR,
        height: SCREEN_HEIGHT * 0.30,
        padding: 60
    },
    formContainer: {
        flex: 1,
        padding: 30
    },
    bottomTextContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 20
    }
});


export default styles;