import React from 'react';
import { StyleSheet } from 'react-native';



export default styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'relative'
    },
    button: {
      backgroundColor: 'blue',
      marginBottom: 10,
    },
    text: {
      color: 'white',
      fontSize: 20,
      textAlign: 'center',
    },
    cameraBtn: {
        height: 60,
        width: 60,
        borderRadius: 40,
        position: 'absolute',
        marginLeft: 10,
        backgroundColor: 'black',
        bottom: 10,                                                    
        right: 10,
        alignItems: 'center',
    },
    icon: {
        marginTop: 9
    },
    topHeaderText:{
        fontSize: 20,
        textAlign: 'center',
    },
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        padding: 10
    },
    selectedImage: {
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',  
        width: '33%',
        height: 100,
    },
    checkedIcon: {
        position: 'absolute',
        //backgroundColor: 'rgba(52, 52, 52, 0.8)'
    }
  });
  