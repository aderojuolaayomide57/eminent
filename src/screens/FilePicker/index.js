import React, { Component } from 'react';
import { Alert, Image, ScrollView, Text, TouchableOpacity, View, FlatList } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Video from 'react-native-video';
import styles from './styles';
import {requestPermissions} from '../../utils/helpers';
import CameraRoll from "@react-native-community/cameraroll";
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import _ from 'lodash';





export default class FilePicker extends Component {
  constructor() {
    super();
    this.state = {
      image: null,
      images: null,
      data: null,
      selectedImages: [],
    };
  }

    componentDidMount(){
        if (Platform.OS === 'android') {
            this.askPermission();
        }
    }

    multipleSelection = (source) => {

        const found = this.state.selectedImages.find(element => element === source);
        if(!found){
            this.state.selectedImages.push(source)
            this.setState({
                selectedImages: this.state.selectedImages
            });
        }else{
            _.remove(this.state.selectedImages, (e)=>{
                return e === source
            })
            this.setState({
                selectedImages: this.state.selectedImages
            });
        }
        
        console.log(this.state.selectedImages)
    }

    askPermission = async () => {
        
        const granted = await requestPermissions();
        if (granted) {
            Alert.alert("Permission have been granted")
        } else {
            Alert.alert("You need to grant permissions to access our services")
        }

        CameraRoll.getPhotos({
            first: 4,
            assetType: 'Photos',
        })
        .then(res => {
            this.setState({ data: res.edges });
        })
        .catch((error) => {
            console.log(error);
        });
    }

    cropMultipleImages = () => {
        ImagePicker.openCropper({
            path: this.state.selectedImages[0],
            width: 300,
            height: 400
        }).then((images) => {
            this.setState({
                image: null,
                images: images.map((i) => {
                console.log('received image', i);
                return {
                    uri: i.path,
                    width: i.width,
                    height: i.height,
                    mime: i.mime,
                };
                }),
            });
            
        });
    }

  pickSingleWithCamera(cropping, mediaType = 'photo') {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
      includeExif: true,
      mediaType,
    })
      .then((image) => {
        console.log('received image', image);
        this.setState({
          image: {
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
          images: null,
        });
      })
      .catch((e) => alert(e));
  }

  pickSingleBase64(cropit) {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      includeBase64: true,
      includeExif: true,
    })
      .then((image) => {
        console.log('received base64 image');
        this.setState({
          image: {
            uri: `data:${image.mime};base64,` + image.data,
            width: image.width,
            height: image.height,
          },
          images: null,
        });
      })
      .catch((e) => alert(e));
  }

  cleanupImages() {
    ImagePicker.clean()
      .then(() => {
        console.log('removed tmp images from tmp directory');
      })
      .catch((e) => {
        alert(e);
      });
  }

  cleanupSingleImage() {
    let image =
      this.state.image ||
      (this.state.images && this.state.images.length
        ? this.state.images[0]
        : null);
    console.log('will cleanup image', image);

    ImagePicker.cleanSingle(image ? image.uri : null)
      .then(() => {
        console.log(`removed tmp image ${image.uri} from tmp directory`);
      })
      .catch((e) => {
        alert(e);
      });
  }

  cropLast() {
    if (!this.state.image) {
      return Alert.alert(
        'No image',
        'Before open cropping only, please select image'
      );
    }

    ImagePicker.openCropper({
      path: this.state.image.uri,
      width: 200,
      height: 200,
    })
      .then((image) => {
        console.log('received cropped image', image);
        this.setState({
          image: {
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
          images: null,
        });
      })
      .catch((e) => {
        console.log(e);
        Alert.alert(e.message ? e.message : e);
      });
  }

  pickSingle(cropit, circular = false, mediaType) {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: circular,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
    })
      .then((image) => {
        console.log('received image', image);
        this.setState({
          image: {
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
          images: null,
        });
      })
      .catch((e) => {
        console.log(e);
        Alert.alert(e.message ? e.message : e);
      });
  }

  pickMultiple() {
    ImagePicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      sortOrder: 'desc',
      includeExif: true,
      forceJpg: true,
    })
      .then((images) => {
        this.setState({
          image: null,
          images: images.map((i) => {
            console.log('received image', i);
            return {
              uri: i.path,
              width: i.width,
              height: i.height,
              mime: i.mime,
            };
          }),
        });
      })
      .catch((e) => alert(e));
  }

  scaledHeight(oldW, oldH, newW) {
    return (oldH / oldW) * newW;
  }

  renderVideo(video) {
    console.log('rendering video');
    return (
      <View style={{ height: 300, width: 300 }}>
        <Video
          source={{ uri: video.uri, type: video.mime }}
          style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0 }}
          rate={1}
          paused={false}
          volume={1}
          muted={false}
          resizeMode={'cover'}
          onError={(e) => console.log(e)}
          onLoad={(load) => console.log(load)}
          repeat={true}
        />
      </View>
    );
  }

  renderImage(image) {
    return (
      <Image
        style={{ width: 300, height: 300, resizeMode: 'contain' }}
        source={image}
      />
    );
  }

  renderAsset(image) {
    if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
            <Text style={styles.topHeaderText}>Gallery</Text>
            {this.state.selectedImages.length === 0 && 
            <AntDesign 
                name="select1"
                color="black"
                style={styles.icon}
                size={25}
            />}
            {this.state.selectedImages.length >= 1 &&
                <TouchableOpacity
                    onPress={() => this.cropMultipleImages()}
                >
                    <Text style={styles.topHeaderText}>
                        OK
                    </Text>
                </TouchableOpacity>
            }
        </View>
        {this.state.image && 
        <ScrollView>
          {this.state.image ? this.renderAsset(this.state.image) : null}
          {this.state.images
            ? this.state.images.map((i) => (
                <View key={i.uri}>{this.renderAsset(i)}</View>
              ))
            : null}
        </ScrollView>}
        
        <FlatList
            data={this.state.data}
            numColumns={3}
            renderItem={({ item, index }) => 
                <TouchableOpacity style={styles.selectedImage}
                    onPress={() => this.multipleSelection(item.node.image.uri)}
                >    
                    <Image
                        style={{
                            width: '100%',
                            height: 100,
                            borderColor: 'white',
                            borderWidth: 2,
                        }}
                        source={{ uri: item.node.image.uri }}
                    />
                    {this.state.selectedImages.find(element => element === item.node.image.uri) && 
                    <AntDesign 
                        name="check"
                        color="white"
                        style={styles.checkedIcon}
                        size={25}
                    />}
                </TouchableOpacity>
            }
            keyExtractor={(item, index) => {
                index.toString();
                //console.log(item.node.image.uri);
            }}
        />



        <TouchableOpacity
          onPress={() => this.pickSingleWithCamera(false)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Single Image With Camera</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            this.pickSingleWithCamera(false, (mediaType = 'video'))
          }
          style={styles.button}
        >
          <Text style={styles.text}>Select Single Video With Camera</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingleWithCamera(true)}
          style={styles.button}
        >
          <Text style={styles.text}>
            Select Single With Camera With Cropping
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingle(false)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Single</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.cropLast()} style={styles.button}>
          <Text style={styles.text}>Crop Last Selected Image</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingleBase64(false)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Single Returning Base64</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingle(true)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Single With Cropping</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingle(true, true)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Single With Circular Cropping</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.pickMultiple.bind(this)}
          style={styles.button}
        >
          <Text style={styles.text}>Select Multiple</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.cleanupImages.bind(this)}
          style={styles.button}
        >
          <Text style={styles.text}>Cleanup All Images</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.cleanupSingleImage.bind(this)}
          style={styles.button}
        >
          <Text style={styles.text}>Cleanup Single Image</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.pickSingleWithCamera(true)}
          style={styles.cameraBtn}
        >
            <Entypo
              name="camera"
              color="white"
              style={styles.icon}
              size={35}
            />
        </TouchableOpacity>
      </View>
    );
  }
}