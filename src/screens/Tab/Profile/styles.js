import { StyleSheet } from 'react-native';
import { 
    BLACK_COLOR, 
    PRIMARY_COLOR, 
    WHITE_COLOR, 
    GREY_BLACK, 
    WHITE_OAT_COLOR, 
    BORDER_COLOR 
} from '../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
    },
    image: {
        alignSelf: 'center',
        width: 150,
        height: 100,
        justifyContent: 'center'  
    },
    imageContainer: {
        flex: 1,
        paddingTop: 185,
        paddingLeft: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 20,

    },
    background: {
        flex: 1,
        resizeMode: "cover",
        width: SCREEN_WIDTH,
        alignSelf: 'center',
        height: SCREEN_HEIGHT * 0.33
    },
    headerText: {
        color: WHITE_COLOR,
    },
    headerSubText: {
        color: WHITE_COLOR,
    },
    headerIconBox: {
        backgroundColor: WHITE_COLOR,
        height: 40,
        width: 40,
        borderRadius: 40,
        justifyContent: 'center',
        padding: 11,
        marginTop: 25
    },
    profileContainer: {
        padding: 20
    },
    walletContainer: {
        borderRadius: 5,
        borderColor: BORDER_COLOR,
        borderWidth: 2,
        padding: 6,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 15,
        backgroundColor: WHITE_OAT_COLOR,
        marginBottom: 10
    },
    walletText: {
        fontWeight: 'bold',
        fontSize: 18
    },
    walletTextPrice: {
        fontSize: 13,
        color: WHITE_COLOR,
        fontWeight: 'bold',
    },
    walletTextPriceBox: {
        backgroundColor: PRIMARY_COLOR,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 0,
        borderRadius: 20,
        height: 25,
        marginTop: 5
    },
    listIcon:{
        height: 30,
        width: 30,
        borderRadius: 3,
        backgroundColor: PRIMARY_COLOR,
        padding: 5,
        marginRight: 10
    },
    listBox: {
        flexDirection: 'row',
        paddingTop: 15
    },
    listInnerBox: {
        flexDirection: 'row',
        borderBottomColor: BORDER_COLOR,
        borderBottomWidth: 2,
        justifyContent: 'space-between',
        width: SCREEN_WIDTH * 0.8,
        paddingBottom: 5,
        paddingTop: 5
    },
    paymentListBox: {
        flexDirection: 'row',
        borderBottomColor: BORDER_COLOR,
        borderBottomWidth: 2,
        justifyContent: 'space-between',
        width: SCREEN_WIDTH * 0.9,
        paddingBottom: 5,
        paddingTop: 5
    }
});


export default styles;