import React from 'react';
import { View, ScrollView, Image, ImageBackground, TouchableOpacity } from 'react-native';
import styles from './styles';
import CustomText from '../../../components/text';
import CustomTextInput from '../../../components/textInput';
import CustomButton from '../../../components/button';
import { PRIMARY_COLOR, BLACK_COLOR, BORDER_COLOR, WHITE_COLOR } from '../../../styles/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';



const AccountList = (props) => {
    return(
        <TouchableOpacity 
            style={styles.listBox}
            onPress={() => {}}
        >
            <View style={styles.listIcon}>
                <MaterialIcons 
                    size={18}
                    name="lock"
                    color={WHITE_COLOR}
                />
            </View>
            <View style={styles.listInnerBox}>
                <CustomText
                    text={props.text}
                    size="sm"
                />
                <MaterialIcons 
                    size={20}
                    name="keyboard-arrow-right"
                    color={BORDER_COLOR}
                />
            </View>
        </TouchableOpacity>
    )
}

const PaymentMethod = (props) => {
    return(
        <View style={styles.paymentListBox}>
            <MaterialIcons 
                size={18}
                name="lock"
                color={PRIMARY_COLOR}
            />
            <CustomText
                text={props.text}
                size="sm"
            />
            <AntDesign 
                size={20}
                name="checkcircle"
                color={PRIMARY_COLOR}
            />
        </View>
    )
}

const Profile = () => {
    return(
        <ScrollView style={styles.container}>
            <ImageBackground source={require("../../../assets/Group894.png")} style={styles.background}>
                <View style={styles.imageContainer}>
                    <View>
                        <CustomText
                            text="Ogoro Lekan"
                            size="lg"
                            style={styles.headerText}
                        />
                        <CustomText
                            text="aderojuolaayomide57@gmail.com"
                            size="sm"
                            style={styles.headerSubText}
                        />
                    </View>
                    <View style={styles.headerIconBox}>
                        <FontAwesome
                            size={18}
                            name="pencil"
                        />
                    </View>
                </View>
            </ImageBackground>
            <View style={styles.profileContainer}>
                <View style={styles.walletContainer}>
                    <CustomText
                        text="Account Credits"
                        size="md"
                        style={styles.walletText}
                    />
                    <View style={styles.walletTextPriceBox}>
                        <CustomText
                            text="#10,000"
                            size="sm"
                            style={styles.walletTextPrice}
                        />
                    </View>
                </View>
                <CustomText
                    text="Account"
                    size="md"
                    style={styles.walletText}
                />
                <View style={{marginBottom: 10}}>
                    <AccountList text="Change Password"/>
                    <AccountList text="Settings"/>
                    <AccountList text="Invite Friends"/>
                    <AccountList text="Log Out"/>
                </View>
                <CustomText
                    text="Payment Method"
                    size="md"
                    style={styles.walletText}
                />
                <View>
                    <PaymentMethod text="Change Password"/>
                </View>
            </View>
        </ScrollView>
    )
}
export default Profile;