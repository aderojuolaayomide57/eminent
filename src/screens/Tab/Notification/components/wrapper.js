import React from 'react';
import { View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import CustomText from '../../../../components/text';
import { CHARCOAL_BLACK, WHITE_COLOR } from '../../../../styles/colors';

const styles = StyleSheet.create({
    scroll: {
        backgroundColor: WHITE_COLOR,
    },
    container: {
        margin: 20
    },
    headerWrapper: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    icon: {
        color: CHARCOAL_BLACK,
        fontSize: 20
    },
    image: {
        height: 40,
        width: 40,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    headerText: {
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 16,
        letterSpacing: 0.33913,
        paddingLeft: 80,
        paddingTop: 5,
        textAlign: 'center',
    },
    childrenWrapper: {
        marginVertical: 10
    }
})

class NotificationWrapper extends React.Component {
    render() {
        return (
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <View style={styles.headerWrapper}>
                        <CustomText
                          size="sm"
                          text={this.props.title}
                          style={styles.headerText}
                        />
                    </View>
                    
                    <View style={styles.childrenWrapper}>
                        {this.props.children}
                    </View>
                </View>
            </ScrollView>

        )
    }
}

export default NotificationWrapper;
