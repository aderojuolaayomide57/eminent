import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import PropTypes from 'prop-types';

import CustomText from '../../../../components/text';
import { SCREEN_WIDTH } from '../../../../styles/base';
import { PRIMARY_COLOR, WHITE_COLOR, CHARCOAL_BLACK } from '../../../../styles/colors';
import Entypo from 'react-native-vector-icons/Entypo';


class Notification extends Component {
    
    constructor(props){
        super(props);
        this.changeSeenStatus = this.changeSeenStatus.bind(this);
        this.state = {
            seenState: this.props.seen
        }
        
    }

    changeSeenStatus = () => {
        //this.setState({seenState: true})
    }


    render() {
        const unseen = "#3291FF" 
        const seen = "#AAB7BC" 
        return (
            <TouchableOpacity onPress={() => {
                //this.changeSeenStatus()
                Alert.alert(this.props.title, this.props.message)
                this.props.updateNotificationStatus(this.props.notification_id)
                }}>
                <View style={styles.imageWrapper}>
                    <Entypo name="dot-single"
                        style={styles.iconDesign} size={60}  color={this.state.seenState ? seen : unseen}
                    />
                    <View style={styles.mainTextWrapper}>
                        <CustomText
                            text={this.props.title}
                            size="md"
                            style={[styles.headerText, { opacity: 0.8}]}
                        />
                    <CustomText
                            text={this.props.message}
                            size="sm"
                            style={[styles.message]}
                        />
                    </View>
                        
                
                </View>
            </TouchableOpacity>
        )
    }
}

Notification.propTypes = {
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    btnText: PropTypes.string,
    onPress: PropTypes.func
}

Notification.defaultProps = {
    btnText: null,
    onPress: null
}

const styles = StyleSheet.create({
    imageWrapper: {
        justifyContent: 'center',
        alignSelf: 'center',
        width: SCREEN_WIDTH * 0.9,
        backgroundColor: WHITE_COLOR,
        shadowColor: PRIMARY_COLOR,
        shadowOffset: {
            width: 2,
            height: 34
        },
        shadowRadius: 5,
        shadowOpacity: 2,
        elevation: 5,
        padding: 0,
        margin: 0,
        flexDirection: 'row',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        justifyContent: 'space-between',
        marginBottom: 15
    },

    mainTextWrapper: {
        padding: 5,
        flex: 1,
    },
   
    headerText: {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 15,
        lineHeight: 16,
        letterSpacing: 0.33913,
        paddingTop: 5,
        textAlign: 'left',
        color: CHARCOAL_BLACK
    },
    message: {
        fontStyle: 'normal',
        fontSize: 13,
        letterSpacing: 0.33913,
        textAlign: 'left',
        flexWrap: 'wrap',
        flex: 1,
    },
    iconDesign: {
        width: 45,
        marginLeft: -12,
        padding: 1,
        marginTop: -15,
    }
})

export default Notification;