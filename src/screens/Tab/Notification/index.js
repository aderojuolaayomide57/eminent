import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import CustomText from '../../../components/text';
import NotificationWrapper from './components/wrapper';
import Notification from './components/notification';



const styles = StyleSheet.create({
    subText: {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'left'
    },
})



class Notifications extends Component{
    
    render() {
        var today = new Date();
        const month = today.toLocaleString('default', { month: 'short' })
        const mon = month.toString().substr(4, 3)
        const day = today.getDate()
        const year = today.getFullYear() % 100;
        return (
            <NotificationWrapper
                title="NOTIFICATIONS"
                {...this.props}
            >
                <CustomText
                    size="sm"
                    text={`${day} ${mon}, ${year}`}
                    style={styles.subText}
                />
                <View style={{ marginVertical: 30 }}>
                        <Notification
                            {...this.props}
                            seen={true}
                            notification_id={1}
                            key={1}
                            title="Payment"
                            message="Payment was successful, Thank you please come back some other time"
                        />
                        <Notification
                            {...this.props}
                            seen={true}
                            notification_id={2}
                            key={2}
                            title="Payment"
                            message="Payment was successful, Thank you please come back some other time"
                        />
                        <Notification
                            {...this.props}
                            seen={true}
                            notification_id={3}
                            key={3}
                            title="Payment"
                            message="Payment was successful, Thank you please come back some other time"
                        />              
                </View>
            </NotificationWrapper>
        )
    }
}

export default Notifications;
