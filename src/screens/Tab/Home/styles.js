import { StyleSheet } from 'react-native';
import { BLACK_COLOR, PRIMARY_COLOR, WHITE_COLOR, GREY_BLACK } from '../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
    },
    image: {
        alignSelf: 'center',
        width: 150,
        height: 100,
        justifyContent: 'center'  
    },
    imageContainer: {
        flex: 1,
        width: SCREEN_WIDTH * 0.70,
        paddingTop: 60,
        paddingLeft: 20
    },
    background: {
        flex: 1,
        resizeMode: "cover",
        width: SCREEN_WIDTH,
        alignSelf: 'center',
        height: SCREEN_HEIGHT * 0.33
    },
    headerText: {
        color: WHITE_COLOR,
    },
    headerSubText: {
        color: WHITE_COLOR,
    },
    topserviceContainer: {
        backgroundColor: "white",
    },
    innerserviceBox: {
        backgroundColor:"white",
    },
    mainServiceBox: {
        width: SCREEN_WIDTH * 0.35,
        height: SCREEN_HEIGHT * 0.20,
        backgroundColor:"white",
        elevation:5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        marginLeft: 15,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 20

    },
    serviceText: {
        padding: 10
    },
    serviceImage: {
        width: SCREEN_WIDTH * 0.35,
        height: SCREEN_HEIGHT * 0.15,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,

    },
    mainProductBox: {
        width: SCREEN_WIDTH * 0.50,
        height: SCREEN_HEIGHT * 0.12,
        backgroundColor:"white",
        elevation: 3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        marginLeft: 15,
        borderRadius: 10,
        marginBottom: 20,
        flexDirection: 'row',
        padding: 20

    },
    productText: {
        fontSize: 12,
        lineHeight: 16,
        fontWeight: 'bold'
    },
    productPriceText: {
        fontSize: 12,
        lineHeight: 16,
        fontWeight: 'bold',
        color: GREY_BLACK
    },
    productImage: {
        width: SCREEN_WIDTH * 0.10,
        height: SCREEN_HEIGHT * 0.08,
        

    },
      
});


export default styles;