import React from 'react';
import { View, ScrollView, Image, ImageBackground, TouchableOpacity } from 'react-native';
import styles from './styles';
import CustomText from '../../../components/text';
import CustomTextInput from '../../../components/textInput';
import CustomButton from '../../../components/button';
import { PRIMARY_COLOR, BLACK_COLOR } from '../../../styles/colors';
import Entypo from 'react-native-vector-icons/Entypo';



const Service = (props) => {
    return(
        <TouchableOpacity style={styles.mainServiceBox}>
            <Image 
                source={props.image}
                style={styles.serviceImage}
            />
            <CustomText
                text={props.text}
                size="sm"
                style={styles.serviceText}
            />    
        </TouchableOpacity>
    )
}

const Product = (props) => {
    return(
        <TouchableOpacity style={styles.mainProductBox}>
            <Image 
                source={props.image}
                style={styles.productImage}
            />
            <View style={{paddingTop: 10, paddingLeft: 25}}>
                <CustomText
                    text={props.text}
                    size="sm"
                    style={styles.productText}
                />
                <View style={{flexDirection: 'row', paddingTop: 5}}>
                    <Entypo 
                        name="star"
                        color={PRIMARY_COLOR}
                        size={15}
                    />
                    <CustomText
                        text={props.price}
                        size="sm"
                        style={styles.productPriceText}
                    />
                </View>
            </View>    
        </TouchableOpacity>
    )
}

const Home = () => {
    return(
        <ScrollView style={styles.container}>
            <ImageBackground source={require("../../../assets/Group894.png")} style={styles.background}>
                <View style={styles.imageContainer}>
                    <CustomText
                        text="Welcome to the eminent Saloon"
                        size="lg"
                        style={styles.headerText}
                    />
                    <CustomText
                        text="Let us connect you to the best artisan around you"
                        size="sm"
                        style={styles.headerSubText}
                    />
                </View>
            </ImageBackground>
            <View style={styles.topserviceContainer}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <CustomText
                        text="Services"
                        size="md"
                        style={{
                            paddingTop: 10,
                            marginLeft: 15,
                            fontWeight: 'bold',
                            color: BLACK_COLOR
                        }}
                    />
                    <CustomButton
                        text="See all"
                        type="link"
                        onPress={() => {}}
                        style={{
                            text: {color: PRIMARY_COLOR}
                        }}
                    />
                </View>
                <ScrollView 
                    horizontal={true}
                    style={styles.innerserviceBox}
                >
                    <Service
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                    />
                    <Service
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                    />
                    <Service
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                    />
                </ScrollView>
            </View>
            <View style={styles.topserviceContainer}>
                <View >
                    <CustomText
                        text="Recommend for you"
                        size="md"
                        style={{
                            marginLeft: 15,
                            fontWeight: 'bold',
                            color: BLACK_COLOR,
                            fontSize: 19
                        }}
                    />
                </View>
                <ScrollView 
                    horizontal={true}
                >
                    <Product
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                        price="#2,000"
                    />
                    <Product
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                        price="#2,000"
                    />
                    
                </ScrollView>
                <ScrollView 
                    horizontal={true}
                >
                    <Product
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                        price="#2,000"
                    />
                    <Product
                        text="Haircut"
                        image={require("../../../assets/Group894.png")}
                        price="#2,000"
                    />
                    
                </ScrollView>
            </View>
        </ScrollView>


    )
}
export default Home;