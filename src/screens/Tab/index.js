import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Home from './Home';
import Appointment from './Appointment';
import Profile from './Profile';
import Chat from './Chat';
import Notification from './Notification';
import ArtisanPage from './Artisan';
import { PRIMARY_COLOR, DISABLED_TEXT } from '../../styles/colors'


const Tab = createBottomTabNavigator();

const  Dashboard = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: PRIMARY_COLOR,
        inactiveTintColor: DISABLED_TEXT,
        labelStyle: {
            fontSize: 14,
            paddingRight: 0,
        },
        style: {
            height: 60,
            borderTopColor: 'transparent'
        },
        tabStyle: {
            paddingTop: 8,
            paddingBottom: 8,


        }
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home-outline" color={color} size={28} />
          ),
        }}
      />
      <Tab.Screen
        name="Notification"
        component={Notification}
        options={{
          tabBarLabel: 'Notification',
          tabBarIcon: ({ color, size }) => (
            <IonIcon name="ios-notifications" color={color} size={22} />
          ),
          //tabBarBadge: 3,
        }}
      />
      <Tab.Screen
        name="Appointment"
        component={Appointment}
        options={{
          tabBarLabel: 'Appointment',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="calendar-range" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="ArtisanPage"
        component={ArtisanPage}
        options={{
          tabBarLabel: 'Artisan',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name="chat-bubble-outline" color={color} size={23} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-outline" color={color} size={28} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}


export default Dashboard;