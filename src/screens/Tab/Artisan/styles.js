import { StyleSheet } from 'react-native';
import { BLACK_COLOR, PRIMARY_COLOR, WHITE_COLOR, GREY_BLACK, CHARCOAL_BLACK } from '../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        padding: 20
    },
    topContainer: {
        flex: 1,
        paddingTop: 20,
    },
    headerWrapper: {
        marginBottom: 10,
    },
    artisanBox: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 20,
        
    },

    headerText: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 30,
        letterSpacing: 0.33913,
        color: BLACK_COLOR,
        textAlign: 'left',
        borderBottomWidth: 2,
        borderBottomColor: PRIMARY_COLOR 

    },
    textBox: {
        borderBottomWidth: 2,
        borderBottomColor: "#ddd",
        alignSelf:'stretch',
        flex: 1
    },
    artisanText: {
        color: BLACK_COLOR,
        fontWeight: 'bold',
    },
    subText: {
        marginLeft: 0,
        paddingLeft: 0,
        textAlign: 'left',
        color: "#ddd",
        fontWeight: 'bold',

    },
    rating: {
        flexDirection: 'row'
    },
    icon: {
        color: PRIMARY_COLOR,
        fontSize: 25,
    },
    image: {
        alignSelf: 'center',
        width: 100,
        height: 100,
        justifyContent: 'center',
        borderRadius: 50,
        marginRight: 10
    },
    artisanOuterBox: {
        marginBottom: 30
    },

      
});


export default styles;