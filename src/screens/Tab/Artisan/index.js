import React from 'react';
import { View, ScrollView, TouchableOpacity, Image } from 'react-native';
import CustomText, {CustomDescriptTionText} from '../../../components/text';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const Artisan = (props) => {
    const { navigate } = props;
    return (
        <TouchableOpacity 
            style={styles.artisanBox}
            onPress={() => navigate('Artisan')}
        >
            <Image 
                source={props.image}
                style={styles.image}
            />
            <View style={styles.textBox}>
                <CustomText
                    text={props.text}
                    size="md"
                    style={styles.artisanText}
                />
                <CustomDescriptTionText
                    text={props.subText}
                    style={styles.subText}
                />
                <View style={styles.rating}>
                    <FontAwesome name="star" style={styles.icon} />
                    <CustomDescriptTionText
                        text={props.rate}
                        style={styles.subText}
                    />
                </View>
            </View>    
        </TouchableOpacity>
    )
}

const ArtisanPage = (props) => {
    const { navigation: { navigate } } = props;
    return(
        <ScrollView style={styles.container}>
            <View style={styles.topContainer}>
              <View style={styles.headerWrapper}>
              <TouchableOpacity onPress={() => props.navigation.navigate('Artisan')}>
                  <CustomText
                    size="lg"
                    text="Artisans"
                    style={styles.headerText}
                  />
                </TouchableOpacity>

              </View>

            </View>
            <View>
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
                <Artisan
                    navigate={navigate}
                    text="King David"
                    subText="Specialist in Afro Cut"
                    rate="4.0"
                    image={require("../../../assets/Group894.png")}
                />
            </View>
        </ScrollView>
    )
}
export default ArtisanPage;