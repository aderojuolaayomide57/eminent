import { StyleSheet } from 'react-native';
import { BLACK_COLOR, PRIMARY_COLOR, WHITE_COLOR, GREY_BLACK, CHARCOAL_BLACK } from '../../../styles/colors';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../styles/base';


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    topContainer: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        paddingTop: 30,
        marginBottom: 15,
    },
    headerWrapper: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    headerText: {
        fontFamily: 'Avenir',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 16,
        letterSpacing: 0.33913,
        paddingLeft: SCREEN_WIDTH * 0.25,
        paddingTop: 5,
        textAlign: 'center',
        color: BLACK_COLOR,

    },
    icon: {
        color: BLACK_COLOR,
        fontSize: 25,
        marginLeft: 20,
    },
    image: {
        alignSelf: 'center',
        width: 150,
        height: 100,
        justifyContent: 'center'  
    },
    imageContainer: {
        flex: 1,
        width: SCREEN_WIDTH * 0.70,
        paddingTop: 60,
        paddingLeft: 20
    },

    time: {
        borderRadius: 20,
        borderColor: PRIMARY_COLOR,
        borderWidth: 2,
        paddingLeft: 10,
        paddingRight: 10,
        color: BLACK_COLOR,
        backgroundColor: WHITE_COLOR,
        marginRight: 8,
        marginBottom: 15,
        
    },
    timeBox: {
        flexDirection: 'row',
        alignItems: "flex-start",
        flexWrap: 'wrap',
        flex: 1,
        marginTop: 15

    }
      
});


export default styles;