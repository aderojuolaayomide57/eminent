import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, KeyboardAvoidingView } from 'react-native';

import InlineDatePicker from '../../../components/DatePicker';
import CustomText from '../../../components/text';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Card from '../../../components/card';
import CustomTextInput from '../../../components/textInput';
import CustomButton from '../../../components/button';
import CustomSelect from '../../../components/select';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


const Time = (props) => (
  <TouchableOpacity style={styles.time} >
    <CustomText text={props.title} size="sm" style={{fontSize: 13}}/>
  </TouchableOpacity>
)

class Appointment extends Component {

    state = {
      year: 2021,
      month: 11,
      date: 16,
      selectedValue: "Haircut"
    };
    setDate = (y, m, d) => {
      this.setState({year: y, month: m, date: d});
    }
    setSelectedValue = (itemValue) => {
      this.setState({selectedValue: itemValue});
    }
    
    render() {
      return (
        <KeyboardAwareScrollView 
          style={styles.container} 
          extraScrollHeight={50} 
          enableOnAndroid
        >
            <View style={styles.topContainer}>
              <View style={styles.headerWrapper}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                      <Icon name="arrow-back" style={styles.icon} />
                  </TouchableOpacity>
                  <CustomText
                    size="sm"
                    text="APPOINTMENT"
                    style={styles.headerText}
                  />
              </View>
            </View>
            <InlineDatePicker onChangeDate = {this.setDate}/>
            <Card>
              <CustomText
                size="sm"
                text="Time"
                style={{fontWeight: 'bold'}}
              />
              <View style={styles.timeBox}>
                  <Time title="8:00am"/>
                  <Time title="9:00am"/>
                  <Time title="10:00am"/>
                  <Time title="11:00am"/>
                  <Time title="12:00am"/>
                  <Time title="1:00pm"/>
                  <Time title="2:00pm"/>
                  <Time title="3:00pm"/>
                  <Time title="4:00pm"/>
                  <Time title="5:00pm"/>
                  <Time title="6:00pm"/>
                  <Time title="7:00pm"/>
                  <Time title="8:00pm"/>
                  <Time title="9:00pm"/>
                  <Time title="10:00pm"/>
              </View>
            </Card>

            <Card>
              <CustomText
                size="sm"
                text="Select Services"
                style={{fontWeight: 'bold', marginBottom: 10}}
              />
              <CustomSelect
                selectedValue={this.state.selectedValue}
                onValueChange={(itemValue, itemIndex) => this.setSelectedValue(itemValue)}
                list={[
                  {label: 'Haircut', value: 'Haircut'},
                  {label: 'Home Service', value: 'Home Service'},
                  {label: 'Barb and Dye', value: 'Barb and Dye'},
              ]}
              />
              <CustomText
                size="sm"
                text="Select Artisan"
                style={{fontWeight: 'bold', marginTop: 15, marginBottom: 10}}
              />
              <CustomTextInput
                name="artisan"
                value=""
                onChangeText={() => {}}
                placeholder="Enter artisan name"
              />
            </Card>
            <CustomButton
              type="primary"
              text="BOOK APPOINTMENT"
              onPress={() => {}}
              style={{
                btn: {marginLeft: 20, marginRight: 20, marginBottom: 20}
              }}
            />
        </KeyboardAwareScrollView>
      );
    }
}



export default Appointment;

