/**
 * @format
 */

import {AppRegistry} from 'react-native';
import codePush from "react-native-code-push";
import EminentApp from './App';
import {name as appName} from './app.json';

let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };

const Eminent = codePush(codePushOptions)(EminentApp)


AppRegistry.registerComponent(appName, () => Eminent);
